/* Controllers */
const usuarioController = require('../controllers/usuario');
const productoController = require('../controllers/producto');
const path = require('path');
var VerifyToken = require('../security/VerifyToken');
var multer = require('multer');
var upload = multer({ dest: './assets/products/' });

const uploadImage = () => {
    const imageStorage = multer.diskStorage({
        destination: (req, file, cb) => { 
            console.log(file)
            cb(null, 'assets/products');
         },
        filename: (req, file, cb) => { 
            cb(null, file.originalname)
        }
    });
    const imageFileFilter = (req, file, cb) => {
        if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
            return cb(new Error('Puede subir solo una imagen'), false);
        }
        cb(null, true);
    };
    return multer({imageFileFilter, imageStorage});
}

module.exports = (app) => {

    /* Autenticación */
    app.post('/api/auth/register', usuarioController.create);
    
    app.get('/api/auth/user', VerifyToken, usuarioController.findUserByToken);

    app.post('/api/auth/login', usuarioController.auth);
    
    app.post('/api/auth/login/admin', usuarioController.authAdmin);

    app.post('/api/auth/logout', usuarioController.logout);

    /* Usuarios */
    app.post('/api/auth/user/all', VerifyToken, usuarioController.list);

    app.get('/api/auth/user/find',VerifyToken,  usuarioController.find);

    app.post('/api/auth/user/remove',VerifyToken,  usuarioController.remove);

    app.post('/api/auth/user/active',VerifyToken,  usuarioController.activate);

    app.post('/api/auth/user/update',VerifyToken,  usuarioController.update);

    app.post('/api/auth/user/roles',VerifyToken,  usuarioController.roles);

    /* Productos */
    app.post('/api/auth/product/add', VerifyToken, uploadImage().array('productImage'),productoController.create);

    app.post('/api/auth/product/all', VerifyToken, productoController.list);

    app.get('/api/auth/product/find',VerifyToken,  productoController.find);

    app.post('/api/auth/product/remove',VerifyToken,  productoController.remove);

    app.post('/api/auth/product/update',VerifyToken, uploadImage().array('productImage'), productoController.update);

    app.post('/api/auth/product/tags',VerifyToken,  productoController.tags);

    //VerifyToken,
    app.get('/assets/products/:image', (req, res) => {
        res.sendFile(path.join(__dirname,'../assets/products/'+req.params.image));
    });
    
};