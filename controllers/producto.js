const Product = require('../models').products;
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var config = require('../security/config');
const promisify = fn => {
    return (...args) => {
        return new Promise((resolve, reject) => {
            function customCallback(err, ...results) {
                if (err) {
                    return reject(err);
                }
                return resolve(results.length === 1 ? results[0] : results)
            }

            args.push(customCallback);
            fn.call(this, ...args);
        })
    }
}
const generateName = (length) =>{   
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
         charactersLength));
   }
   return result;
}

module.exports = {
    create(req, res) {
        let imageName="";
        if(req.files) {
            let file = req.files[0];
            const fs = require('fs');
            imageName='assets/products/'+file.originalname;
            fs.writeFileSync(imageName, file.buffer);
             console.log(file)
        }

       /// console.log(req.body)
        let newProduct = {
            name: req.body.name,
            abstract: req.body.abstract,
            image: imageName,//req.body.image,req.body.image,
            description: req.body.description,
            status: true,
            tags: req.body.type,
            nutritional: req.body.nutritional
        };
        console.log(newProduct)
        return Product.create(newProduct).then(product => {
                res.status(200).send(product);
            })
            .catch(error => {
                //res.status(400).send(error)
                res.status(500).send("Error intente nuevamente.")
            });
    },
    list(_, res) {
        return Product.findAll({
            where: {
                status:true
            }
        })
           .then(product => res.status(200).send(product))
           .catch(error => res.status(400).send(error))
    },
    find (req, res) {
        return Product.findAll({
            where: {
                id: req.body.id,
                status:true
            }
        })
        .then(product => res.status(200).send(product))
        .catch(error => res.status(400).send(error))
    },
    remove(req, res) {
        return Product.update(
            { status: false },
            { where: { id: req.body.id } }
          )
        .then(product => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
    update(req, res) {
        let imageName="";
        if(req.files) {
            let file = req.files[0];
            const fs = require('fs');
            imageName='assets/products/'+file.originalname;
            fs.writeFileSync(imageName, file.buffer);
             console.log(file)
        }

        return Product.update(
            { 
                name: req.body.name,
                abstract: req.body.abstract,
                image: imageName,
                description: req.body.description,
                tags: req.body.type,
                nutritional: req.body.nutritional
             },
            { where: { id: req.body.id } }
          )
        .then(product => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
    tags(req, res) {
        return Product.update(
            { tags: req.body.tags },
            { where: { id: req.body.id } }
          )
        .then(product => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
};