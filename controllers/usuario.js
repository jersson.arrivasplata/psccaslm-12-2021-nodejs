const User = require('../models').users;
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var config = require('../security/config');
const promisify = fn => {
    return (...args) => {
        return new Promise((resolve, reject) => {
            function customCallback(err, ...results) {
                if (err) {
                    return reject(err);
                }
                return resolve(results.length === 1 ? results[0] : results)
            }

            args.push(customCallback);
            fn.call(this, ...args);
        })
    }
}
module.exports = {
    create(req, res) {
        var hashedPassword = bcrypt.hashSync(req.body.password, 8);
        let newUserListing = {
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword,
            roles: req.body.roles,
            status: true
        };
        console.log(newUserListing)
        const response = User.create(newUserListing).then(user => {
                var token = jwt.sign({ id: user.id }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
                console.log(token)
                res.status(200).send({ auth: true, token: token });
            })
            .catch(error => {
                //res.status(400).send(error)
                res.status(500).send("Error intente nuevamente.")
            });
        return response;
    },
    findUserByToken(req, res) {
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'Fallo la verificación del Token.' });

        const response = promisify(jwt.verify)(token, config.secret)
            .then(decoded => {
                console.log(decoded);
                return User.findOne({
                    where: {
                      id: decoded.id,
                      status: true
                    },
                  })

            })
            .then(usuario => {
                res.status(200).send(usuario)
            })
            .catch(err => {
                res.status(500).send({ auth: false, message: 'Fallo la verificación del token.' });
            });
        return response;
    },
    logout(req, res) {
        res.status(200).send({ auth: false, token: null });
    },
    authAdmin(req, res) {
        const response = User.findOne({ 
            where:{
                email: req.body.email,
                status: true,
                roles: 'SUPER_ADMIN'
            }
        })
            .then(user => {
                var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
                if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
                var token = jwt.sign({ id: user.id }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });

                res.status(200).send({ auth: true, token: token });
            })
            .catch(err => {
                res.status(404).send('No se encuentra el usuario');
            });
        return response;
    },
    auth(req, res) {
        const response = User.findOne({ 
            where:{
                email: req.body.email,
                status: true
            }})
            .then(user => {
                var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
                if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
                var token = jwt.sign({ id: user.id }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });

                res.status(200).send({ auth: true, token: token });
            })
            .catch(err => {
                res.status(404).send('No se encuentra el usuario');
            });
        return response;
    },
    list(_, res) {
        return User.findAll({
            where: {
                status:true
            }
        })
           .then(usuario => res.status(200).send(usuario))
           .catch(error => res.status(400).send(error))
    },
    find (req, res) {
        console.log( req.body.name);
        return User.findAll({
            where: {
                name: req.body.name,
                status:true
            }
        })
        .then(usuario => res.status(200).send(usuario))
        .catch(error => res.status(400).send(error))
    },
    remove(req, res) {
        return User.update(
            { status: false },
            { where: { email: req.body.email } }
          )
        .then(data => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
    activate(req, res) {
        return User.update(
            { status: true },
            { where: { email: req.body.email } }
          )
        .then(data => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
    update(req, res) {
        return User.update(
            { name: req.body.name },
            { where: { email: req.body.email } }
          )
        .then(data => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
    roles(req, res) {
        return User.update(
            { roles: req.body.roles },
            { where: { email: req.body.email } }
          )
        .then(data => res.status(200).send({response:true}))
        .catch(error => res.status(400).send(error))
    },
};