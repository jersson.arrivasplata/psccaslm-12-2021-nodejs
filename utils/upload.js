const multer = require("multer")

module.exports.uploadImage = () => {
    const imageStorage = multer.diskStorage({
        destination: (req, file, cb) => { cb(null, 'assets/products'); },
        filename: (req, file, cb) => { cb(null, file.originalname)}
    });
    const iamgeFileFilter = (req, file, cb) => {
        if(!file.originalname.match(/\.(jpg|jpeg|png|gif |PNG)$/)) {
            return cb(new Error('Puede subir solo una imagen'), false);
        }
        cb(null, true);
    };
    return multer({iamgeFileFilter, imageStorage});
}