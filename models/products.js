'use strict';
module.exports = (sequelize, DataTypes) => {
    const products = sequelize.define('products', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        name: {
            allowNull: true,
            type: DataTypes.STRING
        },
        abstract: {
            allowNull: true,
            type: DataTypes.STRING,
        },
        tags: {
            allowNull: true,
            type: DataTypes.STRING
        }, 
        image: {
            allowNull: true,
            type: DataTypes.STRING
        },
        description: {
            allowNull: true,
            type: DataTypes.TEXT
        },
        status: {
            allowNull: true,
            type: DataTypes.INTEGER
        },
        nutritional: {
            allowNull: true,
            type: DataTypes.TEXT
        }
    }, {
        timestamps: true,
        freezeTableName: true,
        tableName: 'products',
        classMethods: {}
    });
    products.associate = function(models) {
        // associations can be defined here
    };
    return products;
};