const express = require('express');
//https://expressjs.com/es/guide/routing.html
const bodyParser = require('body-parser');
// This will be our application entry. We'll setup our server here.
const http = require('http');
// Set up the express app
const app = express();
//var multer = require('multer');
//var upload = multer();
const cors = require('cors');

app.use(cors());
//app.use(upload.array()); 
//app.use(upload.single()); 

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Setup a default catch-all route that sends back a welcome message in JSON format.
require('./routes')(app);

app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to the beginning of nothingness.',
}));
const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);
const server = http.createServer(app);
server.listen(port,'0.0.0.0');
module.exports = app;